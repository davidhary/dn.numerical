# Attributions

* [Facilitated By](#Facilitated-By)
* [Acknowledgments](#Acknowledgments)

<a name="Facilitated-By"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Try Convert]
* [Funduc Search and Replace]

<a name="Acknowledgments"></a>
## Acknowledgments
* [Dapper]
* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow]

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Atomineer Code Documentation]: https://www.atomineerutils.com/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[Jarte RTF Editor]: https://www.jarte.com/ 
[Try Convert]: https://github.com/dotnet/try-convert
[Visual Studio]: https://www.visualstudio.com/
[WiX Toolset]: https://www.wixtoolset.org/
