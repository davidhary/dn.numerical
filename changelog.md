# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.1.9083] - 2024-11-13 Preview 202304
* Update packages.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.
* Increment version to 9083.

## [3.1.8956] - 2024-07-09 Preview 202304
* Reference updated core projects.

## [3.1.8949] - 2023-07-02 Preview 202304
* Update to .NET 8.
* Apply code analysis rules.
* Change MSTest SDK to 3.4.3.
* Use local trace loggers for tests under the MSTest namespace.
* Generate assembly attributes.

## [3.1.8535] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.

## [3.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [3.0.8189] - 2022-06-03
* Use Value Tuples to implement GetHashCode().

## [3.0.8125] - 2022-03-31
* Pass tests in project reference mode.

## [3.0.8119] - 2022-03-25
* Use the new Json application settings base class.

## [3.0.8108] - 2022-03-14
* Package.

Add standard packages and compile.
## [3.0.8069] - 2022-02-03
* Targeting Visual Studio 2022, C# 10 and .NET 6.0. 
* Remove unused references.

## [3.0.7832] - 2021-06-11
Ported to .NET standard.

## [3.0.6795] - 2018-08-09
Created from Algorithms Optima.

## [3.0.6667] - 2018-01-17
Uses upgraded core libraries.

## [2.1.6667] - 2018-04-03
2018 release.

## [1.0.5191] - 2014-03-18
Created.

&copy;  2014 Integrated Scientific Resources, Inc. All rights reserved.

[3.1.9083]: https://bitbucket.org/davidhary/dn.numerical/src/main/
