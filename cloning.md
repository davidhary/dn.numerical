# Cloning

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Json Libraries] - .NET Standard Json libraries
* [Std Libraries] - .NET Standard libraries
* [Tracing Libraries] - .NET Standard Tracing libraries
* [Optima Library] - Numerical optimization
* [IDE Repository] - IDE helper files.
```
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
git clone git@bitbucket.org:davidhary/dn.numerical.git
git clone git@bitbucket.org:davidhary/vs.ide.git
```

Clone the repositories into the following relative path(s) (parents of the .git folder):
```
%vslib%\core\json
%vslib%\core\std
%vslib%\core\tracing
%vslib%\Algorithms\Numerical
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

## Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

## Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Json Libraries]: https://bitbucket.org/davidhary/dn.json
[Std Libraries]: https://bitbucket.org/davidhary/dn.std
[Tracing Libraries]: https://bitbucket.org/davidhary/dn.tracing
[Optima Library]: https://bitbucket.org/davidhary/dn.numerical

[Amoeba Method Optimization]: http://msdn.microsoft.com/en-us/magazine/dn201752.aspx
[Nelder-Mead Method]: https://en.wikipedia.org/wiki/Nelder%E2%80%93Mead_method

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
