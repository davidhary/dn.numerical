# About

cc.isr.Numerical.Optima is a .Net library supporting numerical optimization operations.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

cc.isr.Numerical.Optima is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Numerical Repository].

[Numerical Repository]: https://bitbucket.org/davidhary/dn.numerical

