namespace cc.isr.Numerical.Simplex.Tests;

/// <summary> Test information for the Polynomial Fit Tests. </summary>
/// <remarks> <para>
/// David, 2/12/2018 </para></remarks>
internal sealed class RosenbrockFunctionSimplexTestsInfo : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    public RosenbrockFunctionSimplexTestsInfo()
    { }

    #endregion

    #region " singleton "

    /// <summary>   Creates an instance of the <see cref="RosenbrockFunctionSimplexTestsInfo"/> after restoring the 
    /// application context settings to both the user and all user files. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static RosenbrockFunctionSimplexTestsInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( RosenbrockFunctionSimplexTestsInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        RosenbrockFunctionSimplexTestsInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( RosenbrockFunctionSimplexTestsInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static RosenbrockFunctionSimplexTestsInfo Instance => _instance.Value;

    private static readonly Lazy<RosenbrockFunctionSimplexTestsInfo> _instance = new( CreateInstance, true );

    #endregion

    #region " configuration information "

    private TraceLevel _traceLevel = TraceLevel.Verbose;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    public TraceLevel TraceLevel
    {
        get => this._traceLevel;
        set => _ = this.SetProperty( ref this._traceLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " resenbrock conditions "

    /// <summary> Gets or sets the seed. </summary>
    /// <value> The seed. </value>
    public int Seed { get; set; }

    /// <summary> Gets or sets the number of iterations. </summary>
    /// <value> The number of iterations. </value>
	public int IterationCount { get; set; }

    /// <summary> Gets or sets the objective. </summary>
    /// <value> The objective. </value>
	public double Objective { get; set; }

    /// <summary> Gets or sets the convergence. </summary>
    /// <value> The convergence. </value>
	public double Convergence { get; set; }

    /// <summary> Gets or sets the minimum. </summary>
    /// <value> The minimum value. </value>
	public double Minimum { get; set; }

    /// <summary> Gets or sets the maximum. </summary>
    /// <value> The maximum value. </value>
	public double Maximum { get; set; }

    #endregion
}

