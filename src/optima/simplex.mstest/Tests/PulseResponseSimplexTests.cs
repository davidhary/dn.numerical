using cc.isr.Std.Tests;
using cc.isr.Std.RandomExtensions;
using cc.isr.Std.Cartesian;
using cc.isr.Std.Statistics;
using cc.isr.Std.Tests.Extensions;

namespace cc.isr.Numerical.Simplex.Tests;

/// <summary>    (Unit Test Class) Unit tests the pulse response simplex. </summary>
/// <remarks>	 David, 9/23/2017 </remarks>
[TestClass]
[System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Pending>" )]
public sealed class PulseResponseSimplexTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            if ( Logger is null )
                Console.WriteLine( methodFullName );
            else
                Logger?.LogInformationMultiLineMessage( methodFullName );
        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Console.WriteLine( $"Failed initializing the test class: {ex}" );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    private IDisposable? _loggerScope;

    private LoggerTraceListener<PulseResponseSimplexTests>? _traceListener;

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        if ( Logger is not null )
        {
            this._loggerScope = Logger.BeginScope( this.TestContext?.TestName ?? string.Empty );
            this._traceListener = new LoggerTraceListener<PulseResponseSimplexTests>( Logger );
            _ = Trace.Listeners.Add( this._traceListener );
        }
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>	   
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        Assert.IsNotNull( this._traceListener, nameof( TraceListener ) );
        Assert.IsFalse( this._traceListener.Any( TraceEventType.Error ), $"{nameof( this._traceListener )} should have no {TraceEventType.Error} messages" );
        this._loggerScope?.Dispose();
        this._traceListener?.Dispose();
        Trace.Listeners.Clear();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<PulseResponseSimplexTests>? Logger { get; } = LoggerProvider.CreateLogger<PulseResponseSimplexTests>();

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) 00 logger should be enabled. </summary>
    /// <remarks>   2023-05-31. </remarks>
    [TestMethod]
    public void A00LoggerShouldBeEnabled()
    {
        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );
        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );
    }

    /// <summary>   (Unit Test Method) 01 logger trace listener should have messages. </summary>
    /// <remarks>   2023-06-01. </remarks>
    [TestMethod]
    public void A01LoggerTraceListenerShouldHaveMessages()
    {
        Assert.IsNotNull( this._traceListener, $"{nameof( this._traceListener )} should initialize" );
        Assert.IsTrue( Trace.Listeners.Count > 0, $"{nameof( Trace )} should have non-zero {nameof( Trace.Listeners )}" );
        Trace.TraceInformation( "Testing tracing an info message" ); Trace.Flush();
        Assert.IsTrue( this._traceListener.Any( TraceEventType.Information ), $"{nameof( this._traceListener )} should have {TraceEventType.Error} messages" );

        // no need to report errors for this test.

        this._traceListener?.Clear();
    }

    #endregion

    #region " builders "

    /// <summary> Enumerates build time series in this collection. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    /// <param name="sampleInterval"> The sample interval. </param>
    /// <param name="count">          Number of elements. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process build time series in this collection.
    /// </returns>
    private static List<double> BuildTimeSeries( double sampleInterval, int count )
    {
        List<double> l = [];
        for ( int i = 0, loopTo = count - 1; i <= loopTo; i++ )
            l.Add( i * sampleInterval );
        return l;
    }

    /// <summary> Returns an exponent. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    /// <param name="asymptote">    The asymptote. </param>
    /// <param name="timeConstant"> The time constant. </param>
    /// <param name="timeSeries">   The time series. </param>
    /// <returns> A list of time and amplitude values. </returns>
    private static List<CartesianPoint<double>> BuildExponent( double asymptote, double timeConstant, IEnumerable<double> timeSeries )
    {
        List<CartesianPoint<double>> l = [];
        for ( int i = 0, loopTo = timeSeries.Count() - 1; i <= loopTo; i++ )
        {
            double t = timeSeries.ElementAtOrDefault( i );
            double inverseTau = t / timeConstant;
            double value = asymptote * (1d - Math.Exp( -inverseTau ));
            l.Add( new CartesianPoint<double>( t, value ) );
        }

        return l;
    }

    /// <summary> Enumerates add in this collection. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    /// <param name="timeSeries"> The time series. </param>
    /// <param name="noise">      The noise. </param>
    /// <returns>
    /// An enumerator that allows for each to be used to process add in this collection.
    /// </returns>
    private static List<CartesianPoint<double>> Add( IEnumerable<CartesianPoint<double>> timeSeries, IEnumerable<double> noise )
    {
        List<CartesianPoint<double>> l = [];
        for ( int i = 0, loopTo = timeSeries.Count() - 1; i <= loopTo; i++ )
            l.Add( new CartesianPoint<double>( timeSeries.ElementAtOrDefault( i ).X, timeSeries.ElementAtOrDefault( i ).Y + noise.ElementAtOrDefault( i ) ) );
        return l;
    }

    /// <summary> Builds an exponent using integration. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    /// <param name="asymptote">    The asymptote. </param>
    /// <param name="timeConstant"> The time constant. </param>
    /// <param name="count">        Number of. </param>
    /// <returns> A list of. </returns>
    private static List<CartesianPoint<double>> Integrate( double asymptote, double timeConstant, int count )
    {
        List<CartesianPoint<double>> l = [];
        double vc = 0d;
        double deltaT = 0.0001d;
        _ = deltaT * (asymptote - vc) / timeConstant;
        for ( int i = 0, loopTo = count - 1; i <= loopTo; i++ )
        {
            l.Add( new CartesianPoint<double>( ( float ) (deltaT * (i + 1)), ( float ) vc ) );
            double deltaV = deltaT * (asymptote - vc) / timeConstant;
            vc += deltaV;
        }

        return l;
    }

    #endregion

    #region " tests "

    /// <summary> (Unit Test Method) tests exponent simplex. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    [TestMethod()]
    public void ExponentSimplexTest()
    {
        double asymptote = PulseResponseSimplexTestsInfo.Instance.Asymptote;
        int dataPoints = PulseResponseSimplexTestsInfo.Instance.DataPoints;
        double timeConstant = PulseResponseSimplexTestsInfo.Instance.TimeConstant;
        Random rnd = Optima.Solution.Random;
        double expectedAsymptote = asymptote * rnd.NextUniform( 0.9d, 1.1d );
        double expectedTimeConstant = timeConstant * rnd.NextUniform( 0.9d, 1.1d );

        // model initialization parameters
        double[] asymptoteRange = [0.5d * asymptote, 2d * asymptote];
        double[] negativeInverseTauRange = [-2 / timeConstant, -0.5d / timeConstant];
        List<double> noise = new( rnd.Normal( dataPoints, 0d, PulseResponseSimplexTestsInfo.Instance.RelativeNoiseLevel * asymptote ) );
        SampleStatistics noiseSample = new();
        noiseSample.AddValues( [.. noise] );
        noiseSample.Evaluate();
        List<double> timeSeries = new( BuildTimeSeries( PulseResponseSimplexTestsInfo.Instance.SamplingInterval, PulseResponseSimplexTestsInfo.Instance.DataPoints ) );
        List<CartesianPoint<double>> exponent = new( BuildExponent( expectedAsymptote, expectedTimeConstant, timeSeries ) );
        List<CartesianPoint<double>> testData = new( Add( exponent, noise ) );
        Optima.PulseResponseFunction model = new( testData ) { ObjectiveFunctionMode = PulseResponseSimplexTestsInfo.Instance.ObjectiveFunctionMode };
        double expectedMaximumSSQ = noiseSample.SumSquareDeviations;
        double objectivePrecision = 0d;
        double asymptoteAccuracy = PulseResponseSimplexTestsInfo.Instance.AsymptoteAccuracy * expectedAsymptote;
        double inverseTauAccuracy = PulseResponseSimplexTestsInfo.Instance.TimeConstantAccuracy / expectedTimeConstant;
        SampleStatistics testSample = new();
        foreach ( CartesianPoint<double> x in testData )
            testSample.AddValue( x.Y );
        SampleStatistics exponentSample = new();
        foreach ( CartesianPoint<double> x in exponent )
            exponentSample.AddValue( x.Y );
        exponentSample.CastToArray();
        double expectedCorrelationCoefficient = testSample.EvaluateCorrelationCoefficient( exponentSample.ValuesArray() );
        if ( model.ObjectiveFunctionMode == Optima.ObjectiveFunctionMode.Deviations )
            objectivePrecision = PulseResponseSimplexTestsInfo.Instance.RelativeObjectiveLimit * expectedMaximumSSQ;
        else if ( model.ObjectiveFunctionMode == Optima.ObjectiveFunctionMode.Correlation )
            objectivePrecision = PulseResponseSimplexTestsInfo.Instance.RelativeObjectiveLimit * (1d - expectedCorrelationCoefficient);

        Optima.Simplex simplex;
        int dimension = 2;
        simplex = new Optima.Simplex( "Exponent", dimension, [asymptoteRange[0], negativeInverseTauRange[0]], [asymptoteRange[1], negativeInverseTauRange[1]], PulseResponseSimplexTestsInfo.Instance.IterationCount, [PulseResponseSimplexTestsInfo.Instance.RelativeConvergenceRadius * asymptoteAccuracy, PulseResponseSimplexTestsInfo.Instance.RelativeConvergenceRadius * inverseTauAccuracy], objectivePrecision );
        simplex.Initialize( Optima.Solution.Random, model );
        string initialSimplex = simplex.ToString();
        // simplex.Solve()
        _ = simplex.Solve( () => simplex.HitCountOverflow() || simplex.Converged() && simplex.Optimized() );

        // update the model function values
        _ = model.EvaluateObjective( simplex.BestSolution.Values() );
        double actualAsymptote = simplex.BestSolution.Values().ElementAtOrDefault( 0 );
        double actualTimeConstant = -1 / simplex.BestSolution.Values().ElementAtOrDefault( 1 );
        Trace.TraceInformation( $"Expected  exponent {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})" );
        Trace.TraceInformation( $"Estimated exponent {actualAsymptote:G4}(1-exp(-t/{actualTimeConstant:G4})" );
        Trace.TraceInformation( $"Initial simplex:{Environment.NewLine}{initialSimplex}" );
        Trace.TraceInformation( $" Final simplex:{Environment.NewLine}{simplex}" );
        Trace.TraceInformation( $"Best solution {simplex.BestSolution} found after {simplex.IterationNumber} iterations" );
        Trace.TraceInformation( $"Best solution Objective: {simplex.BestSolution.Objective:G4}; Desired: {objectivePrecision:G4}" );
        // Trace.TraceInformation( "Expected Asymptote = {0:G4}", expectedAsymptote)
        // Trace.TraceInformation( "Expected Time Constant = {0:G4}", expectedTimeConstant)
        // Trace.TraceInformation( "Estimated Time Constant = {0:G4}", actualTimeConstant)
        Trace.TraceInformation( "Correlation Coefficient = {0:G5}", model.EvaluateCorrelationCoefficient() );
        Trace.TraceInformation( "   Expected Coefficient = {0:G5}", expectedCorrelationCoefficient );
        Trace.TraceInformation( "         Standard Error = {0:G4}", model.EvaluateStandardError( simplex.BestSolution.Objective ) );
        // Trace.TraceInformation( "Simulated Noise = {0:G4}", noiseSample.Sigma)
        Trace.TraceInformation( "Simulated SSQ = {0:G4}", noiseSample.SumSquareDeviations );
        Trace.TraceInformation( "    Model SSQ = {0:G4}", model.EvaluateSquareDeviations() );
        Trace.TraceInformation( $"Converged: {simplex.Converged()}" );
        Trace.TraceInformation( $"Optimized: {simplex.Optimized()}" );

        // often, the expected solution resides outside the simplex convergence region between the best
        // and worst simplex nodes. Consequently, the distance between the best and solution and
        // expected value may exceed the convergence precision. This the expected precision cannot be
        // used to predict the success of the unit test. 
        Assert.AreEqual( expectedAsymptote, actualAsymptote, PulseResponseSimplexTestsInfo.Instance.AsymptoteAccuracy * expectedAsymptote, $"Asymptote failed; Converged: {simplex.Converged()}; Optimized: {simplex.Optimized()}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})" );
        Assert.AreEqual( expectedTimeConstant, actualTimeConstant, PulseResponseSimplexTestsInfo.Instance.TimeConstantAccuracy * expectedTimeConstant, $"Time constant failed; Converged: {simplex.Converged()}; Optimized: {simplex.Optimized()}; Expected {expectedAsymptote:G4}(1-exp(-t/{expectedTimeConstant:G4})" );
    }

    #endregion
}
