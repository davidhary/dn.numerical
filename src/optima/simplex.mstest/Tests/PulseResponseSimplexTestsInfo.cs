namespace cc.isr.Numerical.Simplex.Tests;

/// <summary> Test information for the Polynomial Fit Tests. </summary>
/// <remarks> David, 2/12/2018 </remarks>
internal sealed class PulseResponseSimplexTestsInfo : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    public PulseResponseSimplexTestsInfo()
    { }

    #endregion

    #region " singleton "

    /// <summary>   Creates an instance of the <see cref="PulseResponseSimplexTestsInfo"/> after restoring the 
    /// application context settings to both the user and all user files. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The new instance. </returns>
    private static PulseResponseSimplexTestsInfo CreateInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( PulseResponseSimplexTestsInfo ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        PulseResponseSimplexTestsInfo ti = new();
        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( PulseResponseSimplexTestsInfo ), ti );

        return ti;
    }

    /// <summary>   Gets the instance. </summary>
    /// <value> The instance. </value>
    public static PulseResponseSimplexTestsInfo Instance => _instance.Value;

    private static readonly Lazy<PulseResponseSimplexTestsInfo> _instance = new( CreateInstance, true );

    #endregion

    #region " configuration information "

    private TraceLevel _messageLevel = TraceLevel.Off;

    /// <summary>   Gets or sets the message trace level. </summary>
    /// <remarks>
    /// This property name is different from the <see cref="Text.Json"/> property name in
    /// order to ensure that the class is correctly serialized. It's value is initialized as <see cref="TraceLevel.Off"/>
    /// in order to test the reading from the settings file.
    /// </remarks>
    /// <value> The message <see cref="TraceLevel"/>. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    [JsonPropertyName( "TraceLevel" )]
    public TraceLevel MessageLevel
    {
        get => this._messageLevel;
        set => _ = this.SetProperty( ref this._messageLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
	[System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
	[System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " exponenet settings "

    /// <summary> Gets or sets the asymptote. </summary>
    /// <value> The asymptote. </value>
    public double Asymptote { get; set; }

    /// <summary> Gets or sets the time constant. </summary>
    /// <value> The time constant. </value>
	public double TimeConstant { get; set; }

    /// <summary> Gets or sets the data points. </summary>
    /// <value> The data points. </value>
	public int DataPoints { get; set; }

    /// <summary> Gets or sets the sampling interval. </summary>
    /// <value> The sampling interval. </value>
	public double SamplingInterval { get; set; }

    /// <summary> Gets or sets the relative noise level. </summary>
    /// <value> The relative noise level. </value>
	public double RelativeNoiseLevel { get; set; }

    #endregion

    #region " convergence conditions "

    /// <summary> Gets or sets the objective function mode. </summary>
    /// <value> The objective function mode. </value>
    public Optima.ObjectiveFunctionMode ObjectiveFunctionMode { get; set; }

    /// <summary> Gets or sets the number of iterations. </summary>
    /// <value> The number of iterations. </value>
	public int IterationCount { get; set; }

    /// <summary>
    /// Gets or sets the relative convergence radius. This is the amount by which the convergence
    /// radius is compressed to help ensure achieving the convergence accuracy in case the simplex
    /// converges from outside the expected values.
    /// </summary>
    /// <value> The relative convergence radius. </value>
	public double RelativeConvergenceRadius { get; set; }

    /// <summary> Gets or sets the Asymptote accuracy. </summary>
    /// <remarks>
    /// The Asymptote accuracy must be greater than the relative noise level for the unit test to
    /// pass because the amplitude may change by the noise level.
    /// </remarks>
    /// <value> The amplitude accuracy. </value>
	public double AsymptoteAccuracy { get; set; }

    /// <summary> Gets or sets the time constant accuracy. </summary>
    /// <value> The time constant accuracy. </value>
	public double TimeConstantAccuracy { get; set; }

    /// <summary> Gets or sets the relative objective limit. </summary>
    /// <value> The relative objective limit. </value>
	public double RelativeObjectiveLimit { get; set; }

    #endregion
}
