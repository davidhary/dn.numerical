using cc.isr.Std.Tests;
using cc.isr.Std.Tests.Extensions;

namespace cc.isr.Numerical.Simplex.Tests;

/// <summary>  (Unit Test Class) Unit tests the Rosenbrock Function Simplex.
/// </summary>
/// <remarks> David, 2020-10-09. </remarks>
[TestClass]
[System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Pending>" )]
public sealed class RosenbrockFunctionSimplexTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            if ( Logger is null )
                Console.WriteLine( methodFullName );
            else
                Logger?.LogInformationMultiLineMessage( methodFullName );
        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Console.WriteLine( $"Failed initializing the test class: {ex}" );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    private IDisposable? _loggerScope;

    private LoggerTraceListener<RosenbrockFunctionSimplexTests>? _traceListener;

    /// <summary> Initializes the test class instance before each test runs. </summary>											   
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        if ( Logger is not null )
        {
            this._loggerScope = Logger.BeginScope( this.TestContext?.TestName ?? string.Empty );
            this._traceListener = new LoggerTraceListener<RosenbrockFunctionSimplexTests>( Logger );
            _ = Trace.Listeners.Add( this._traceListener );
        }
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>	   
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        Assert.IsNotNull( this._traceListener, nameof( TraceListener ) );
        Assert.IsFalse( this._traceListener.Any( TraceEventType.Error ), $"{nameof( this._traceListener )} should have no {TraceEventType.Error} messages" );
        this._loggerScope?.Dispose();
        this._traceListener?.Dispose();
        Trace.Listeners.Clear();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<RosenbrockFunctionSimplexTests>? Logger { get; } = LoggerProvider.CreateLogger<RosenbrockFunctionSimplexTests>();

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) 00 logger should be enabled. </summary>
    /// <remarks>   2023-05-31. </remarks>
    [TestMethod]
    public void A00LoggerShouldBeEnabled()
    {
        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );
        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );
    }

    /// <summary>   (Unit Test Method) 01 logger trace listener should have messages. </summary>
    /// <remarks>   2023-06-01. </remarks>
    [TestMethod]
    public void A01LoggerTraceListenerShouldHaveMessages()
    {
        Assert.IsNotNull( this._traceListener, $"{nameof( this._traceListener )} should initialize" );
        Assert.IsTrue( Trace.Listeners.Count > 0, $"{nameof( Trace )} should have non-zero {nameof( Trace.Listeners )}" );
        Trace.TraceInformation( "Testing tracing an info message" ); Trace.Flush();
        Assert.IsTrue( this._traceListener.Any( TraceEventType.Information ), $"{nameof( this._traceListener )} should have {TraceEventType.Error} messages" );

        // no need to report errors for this test.

        this._traceListener?.Clear();
    }

    #endregion

    #region " solver "

    /// <summary> A test for Solver. </summary>
    /// <remarks> David, 2020-10-09. </remarks>
    [TestMethod()]
    public void RosenbrockFunctionMinimization()
    {
        Trace.TraceInformation( "Begin simplex method optimization demo" );
        Trace.TraceInformation( "Solving Rosenbrock function f(x,y) = 100*(y-x^2)^2 + (1-x)^2" );
        Trace.TraceInformation( "Function Has a minimum at x = 1.0, y = 1.0 when f = 0.0" );
        int dimension = 2; // problem dimension (number of variables to solve for)
        double minX = RosenbrockFunctionSimplexTestsInfo.Instance.Minimum;
        double maxX = RosenbrockFunctionSimplexTestsInfo.Instance.Maximum;
        int maxLoop = RosenbrockFunctionSimplexTestsInfo.Instance.IterationCount;
        double objectivePrecision = RosenbrockFunctionSimplexTestsInfo.Instance.Objective;
        double valuesPrecision = RosenbrockFunctionSimplexTestsInfo.Instance.Convergence;

        // an simplex method optimization solver
        Optima.Simplex simplex = new( "Rosenbrock", dimension, minX, maxX, maxLoop, valuesPrecision, objectivePrecision );
        simplex.Initialize( Optima.Solution.Random, new Optima.RosenbrockObjectiveFunction() );
        Trace.TraceInformation( "Initial simplex is: " );
        Trace.TraceInformation( simplex.ToString() );
        Optima.Solution sln = simplex.Solve();
        Trace.TraceInformation( "Final simplex is: " );
        Trace.TraceInformation( simplex.ToString() );
        Trace.TraceInformation( "Best solution found after {0} iterations: ", simplex.IterationNumber );
        Trace.TraceInformation( sln.ToString() );
        Trace.TraceInformation( "End simplex method optimization test" );
        double actual = sln.Objective;
        double expected = 0d;
        Assert.AreEqual( expected, actual, objectivePrecision );
    }
    #endregion
}
