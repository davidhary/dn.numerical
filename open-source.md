# Open Source

Libraries for numerical analysis.  Include [Nelder-Mead Method] optimization.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open source
Open source used by this software is described and licensed at the
following sites:  
[Json Libraries]  
[Std Libraries]   
[Tracing Libraries]  
[Optima Library]  
[Amoeba Method Optimization]  

<a name="Closed-software"></a>
## Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Std Libraries]

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Json Libraries]: https://bitbucket.org/davidhary/dn.json
[Std Libraries]: https://bitbucket.org/davidhary/dn.std
[Tracing Libraries]: https://bitbucket.org/davidhary/dn.tracing
[Optima Library]: https://bitbucket.org/davidhary/dn.numerical

[Amoeba Method Optimization]: http://msdn.microsoft.com/en-us/magazine/dn201752.aspx
[Nelder-Mead Method]: https://en.wikipedia.org/wiki/Nelder%E2%80%93Mead_method

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[SQL Server Express]: https://www.microsoft.com/en-us/sql-server/sql-server-downloads
[SQL Server Compact]: http://www.microsoft.com/sqlserver/en/us/editions/2012-editions/compact.aspx
[SQLite]: https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki

